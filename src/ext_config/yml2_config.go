package main

import (
  "fmt"
  "gopkg.in/yaml.v2"
  "io/ioutil"
)


type EnvConfig struct {
  Adapter string
  Encoding string
  Pool int
  Username string
  Password string
}

type DbConfigs struct {
  DbConfig map[string] EnvConfig
}


func check_error(err error) {
  if err != nil {
    panic(err)
  }
}

func main() {
  source, err := ioutil.ReadFile("./database.yml")
  check_error(err)
  fmt.Printf("%v.\n", source)

  var configs DbConfigs
  err = yaml.Unmarshal(source, &configs)
  check_error(err)

  fmt.Printf("%v.\n", configs)
}
