package main

import (
  "fmt"
  "github.com/kylelemons/go-gypsy/yaml"
)

func NodeToMap(node yaml.Node) (yaml.Map) {
    mp, ok := node.(yaml.Map)
    if !ok {
        panic(fmt.Sprintf("%v is not of type map", node))
    }
    return mp
}

func NodeToList(node yaml.Node) (yaml.List) {
    lst, ok := node.(yaml.List)
    if !ok {
        panic(fmt.Sprintf("%v is not of type list", node))
    }
    return lst
}

func CheckError(err error) {
  if err != nil {
    panic(err)
  }
}

func main() {
  file, err := yaml.ReadFile("database.yml")
  CheckError(err)

  value := NodeToMap(NodeToMap(file.Root)["default"])
  fmt.Printf("Value: %#v\n", value)
  fmt.Printf("Adapter: %#v\n", value["adapter"])



}
