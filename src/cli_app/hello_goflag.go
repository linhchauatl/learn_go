package main

import (
  "fmt"
  flags "github.com/jessevdk/go-flags"
)

var opts struct {
  Name string `short:"n" long:"name" default:"World!" description:"A name to say hello to."`
  Spanish bool `short:"s" long:"spanish" description:"Use Spanish language."`
}

func main() {
  flags.Parse(&opts)
  fmt.Printf("Name = %s, Spanish = %v.\n", opts.Name, opts.Spanish)

  if opts.Spanish == true {
    fmt.Printf("Holla %s!\n", opts.Name)
  } else {
    fmt.Printf("Hello %s!\n", opts.Name)
  }
}
