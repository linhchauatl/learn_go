package main

import (
  "fmt"
  "os"
  "gopkg.in/urfave/cli.v1"
)

func main() {
  app := cli.NewApp()
  app.Name  = "hello_cli"
  app.Usage = "Println Hello World."
  app.Flags = []cli.Flag {
    cli.StringFlag {
      Name: "name, n",
      Value: "World!",
      Usage: "Who to say hello to.",
    },
    cli.BoolFlag {
      Name: "spanish, s",
      Usage: "Use Spanish language.",
    },
  }

  app.Action = func(c *cli.Context) error {
    name := c.GlobalString("name")
    spanish := c.GlobalBool("spanish")

    if spanish {
      fmt.Printf("Holla %s!\n", name)
    } else {
      fmt.Printf("Hello %s!\n", name)
    }
    return nil
  }

  app.Run(os.Args)
}
