package main

import (
  "fmt"
  "os"
  "bufio"
)

func main() {
	counts := make(map[string]int)
  files := os.Args[1:]

  if len(files) == 0 {
    CountLines(os.Stdin, counts)
  } else {
    for _, arg := range files {
      f, err := os.Open(arg)
      if err != nil {
        fmt.Fprintf(os.Stderr, "dup_2: %v\n", err)
        continue
      }

      CountLines(f, counts)
      f.Close()
    }
  }

  for text, n := range counts {
    if n > 1 {
      fmt.Printf("%d\t%s\n", n, text)
    }
  }
}


func CountLines(f *os.File, counts map[string]int)  {
  input := bufio.NewScanner(f)
  for input.Scan() {
    counts[input.Text()]++
  }
  // ignore potential errors input.Err()
}
