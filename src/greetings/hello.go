package main

import "fmt"

func GetName() string {
	return "World!"
}

func main() {
	fmt.Println("Hello, ", GetName())
}
