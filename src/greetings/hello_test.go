package main

import "testing"

func TestGetName(t *testing.T) {
	name := GetName()

	if name != "World!" {
		t.Error("Response from GetName is unexpected value.")
	}
}
