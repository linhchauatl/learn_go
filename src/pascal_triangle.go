package main

import (
  "fmt"
  "os"
  "bufio"
  "strconv"
)

func main()  {
  fmt.Printf("Enter the number of row: ")
  input := bufio.NewScanner(os.Stdin)
  input.Scan()
  rows, _ := strconv.Atoi(input.Text())
  fmt.Println("rows = ", rows)

  // arr is to hold one row and initialize the first row
  arr := []byte {1}

  for i := 0; i < rows; i++ {
    // print row i
    for j := 0; j < len(arr); j++ {
      fmt.Printf("%d ", arr[j])
    }
    fmt.Printf("\n")

    // initialize the next row
    newArr := []byte {1}
    for j:=0;  j < (len(arr) - 1); j++ {
      newArr = append(newArr, (arr[j] + arr[j+1] ) )
    }
    newArr = append(newArr, 1)
    arr = newArr
  }

}
