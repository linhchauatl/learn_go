/*
go get ./...
walk through the codebase and get any referenced external packages.
*/
package main

import (
  "fmt"
  "time"
)

func PrintCount(c chan int) {
  num := 0
  for num >= 0 {
    num = <- c
    fmt.Print(num, " ")
  }

  time.Sleep(time.Millisecond * 2000)
}

func main() {
  c := make(chan int)
  a := []int {9, 8, 7, 6, 5, 4, 3, 2, 1}

  go PrintCount(c)

  for _, v := range a {
    c <- v
  }

  time.Sleep(time.Millisecond * 1)
  fmt.Println("End of main.")
}
