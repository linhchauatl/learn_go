package main

import (
  "fmt"
  "os"
  "strconv"
  "strings"
)

func main() {
  var str, sep string
  for idx, arg := range(os.Args[1:]) {
    str += sep + arg + sep + strconv.Itoa(idx)
    sep = " "	
  }
  
  fmt.Println(str)
  fmt.Println(strings.Join(os.Args, "_"))
}